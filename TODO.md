Pengembangan Selanjutnya.
Pengerjaan kira-kira 16 jam.
- [done] Harus support untuk semua CARDINALITY (satu atau unlimited).
- [done] Perlu perhatian pada function file_field_widget_submit.
  Karena function tersebut melakukan perubahan field_state, reindex delta,
  yang nanti diikuti oleh file_field_widget_form.
  file_field_widget_form nanti mengikuti perubahan field_state.
- [done] Harus support untuk non javascript.
- [done] Hasil analisis, meski tombol remove ditekan, jangan sampai field_state
  mengubah 'items'. Artinya harus tetep exists. Sehingga nantinya berpengaruh
  dengan default value.
  remove button cukup mengubah fid. pada element. Berarti nantinya perlu
  kembali menggunakan entity_field_presave.
- Masih error pada single value. Perlu dibedakan sejak awal perlakuan antara
  single value dan multi value terutama terkait storage di field state.
  Pada kasus: 
  - edit content existing
  - single value
  - do: remove, upload baru, nah harusnya disamping
    tombol remove perlu ada lagi tombol undo.
    ini jadi pe er.
    solusinya adalah saat init, perlu ditambah lagi 
    di field state sbb
    file_undo_button][item] = item single saat init.
    nanti begitu upload_button trigger, cek sehingga
    perlu dishow undo button.